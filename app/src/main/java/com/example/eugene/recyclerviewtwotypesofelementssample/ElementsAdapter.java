package com.example.eugene.recyclerviewtwotypesofelementssample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eugene.recyclerviewtwotypesofelementssample.entities.BigElement;
import com.example.eugene.recyclerviewtwotypesofelementssample.entities.SmallElement;

import java.util.List;

public class ElementsAdapter extends RecyclerView.Adapter<ElementsAdapter.ViewHolder> {

    private List<BigElement> data;

    public ElementsAdapter(List<BigElement> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (viewType == BIG_ELEMENT_VIEW_TYPE) {
            view = inflater.inflate(R.layout.li_big_element, parent, false);
        } else {
            view = inflater.inflate(R.layout.li_element, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String name = getElementHolder(position).name;
        holder.tvName.setText(name);
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Clicked element with name " + name, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private ElementHolder getElementHolder(int position) {
        int currentPosition = 0;
        for (BigElement element : data) {
            if (position <= currentPosition + element.getElements().size()) {
                if (position - currentPosition == 0) {
                    return new ElementHolder(element.getName(), BIG_ELEMENT_VIEW_TYPE);
                }
                SmallElement smallElement = element.getElements().get(position - currentPosition - 1);
                return new ElementHolder(smallElement.getName(), SMALL_ELEMENT_VIEW_TYPE);
            } else {
                currentPosition += element.getElements().size() + 1; // + 1 for eah big element
            }
        }
        throw new IllegalArgumentException("No element at such position");
    }

    @Override
    public int getItemCount() {
        int count = data.size();
        for (BigElement element : data) {
            count += element.getElements().size();
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        return getElementHolder(position).viewType;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.element_name);
        }
    }

    static class ElementHolder {

        final String name;
        final int viewType;

        public ElementHolder(String name, int viewType) {
            this.name = name;
            this.viewType = viewType;
        }
    }

    private static int BIG_ELEMENT_VIEW_TYPE = 0; //make it enum @IntDef
    private static int SMALL_ELEMENT_VIEW_TYPE = 1;
}
