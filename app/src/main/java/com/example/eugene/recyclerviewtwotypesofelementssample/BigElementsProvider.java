package com.example.eugene.recyclerviewtwotypesofelementssample;

import com.example.eugene.recyclerviewtwotypesofelementssample.entities.BigElement;
import com.example.eugene.recyclerviewtwotypesofelementssample.entities.SmallElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BigElementsProvider implements ElementsProvider {

    @Override
    public List<BigElement> getElements() {
        String[] names = new String[] {"AAA", "BBB", "CCC", "DDDD", "FEFEF"};
        List<BigElement> elements = new ArrayList<>(5);
        for(int i = 0; i < 5; i++) {
            elements.add(getBigElement(names[i], getSmallElements(names[i].substring(2))));
        }
        return elements;
    }

    private static List<SmallElement> getSmallElements(String name) {
        Random random = new Random();
        int size= random.nextInt(7) + 1; //to have at least one element
        List<SmallElement> elements = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            elements.add(new SmallElement("Element" + name + " " + i));
        }
        return elements;
    }

    private static BigElement getBigElement(String name, List<SmallElement> smallElements) {
       return new BigElement(name, smallElements);
    }

}
