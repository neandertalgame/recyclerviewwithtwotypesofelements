package com.example.eugene.recyclerviewtwotypesofelementssample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView elements = (RecyclerView) findViewById(R.id.rv_elements);
        elements.setLayoutManager(new LinearLayoutManager(this));
        elements.setAdapter(new ElementsAdapter(new BigElementsProvider().getElements()));
    }
}
