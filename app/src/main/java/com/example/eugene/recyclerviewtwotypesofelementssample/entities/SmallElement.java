package com.example.eugene.recyclerviewtwotypesofelementssample.entities;

public class SmallElement {

    private final String name;

    public SmallElement(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
