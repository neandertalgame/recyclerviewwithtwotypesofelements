package com.example.eugene.recyclerviewtwotypesofelementssample;

import com.example.eugene.recyclerviewtwotypesofelementssample.entities.BigElement;

import java.util.List;

public interface ElementsProvider {

    List<BigElement> getElements();
}
