package com.example.eugene.recyclerviewtwotypesofelementssample.entities;

import java.util.List;

public class BigElement {

    private final String name;
    private final List<SmallElement> elements;

    public BigElement(String name, List<SmallElement> elements) {
        this.name = name;
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public List<SmallElement> getElements() {
        return elements;
    }
}
